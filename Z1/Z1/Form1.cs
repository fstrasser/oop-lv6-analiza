﻿/* Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
 * znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
 * naprednih (sin, cos, log, sqrt...) operacija. 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Z1
{
    public partial class Form1 : Form
    {
        double broj1, broj2;
        int opcija = -1;

        public Form1()
        {
            InitializeComponent();
        }

        private void sinus_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Text = Math.Sin(broj1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void cosinus_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Text = Math.Cos(broj1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void tangens_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Text = Math.Tan(broj1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void cotangens_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Text = (1 / Math.Tan(broj1)).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void apsolutna_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Text = Math.Abs(broj1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Clear();
                opcija = 0;
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void minus_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Clear();
                opcija = 1;
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void puta_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Clear();
                opcija = 2;
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void podijeljeno_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Clear();
                opcija = 3;
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out broj2))
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
                opcija = -1;
            }
            if (opcija == -1) textBox1.Clear();
            switch (opcija)
            {
                case 0:
                    textBox1.Text = (broj1 + broj2).ToString();
                    break;
                case 1:
                    textBox1.Text = (broj1 - broj2).ToString();
                    break;
                case 2:
                    textBox1.Text = (broj1 * broj2).ToString();
                    break;
                case 3:
                    textBox1.Text = (broj1 / broj2).ToString();
                    break;
            }
            opcija = -1;
        }

        private void korjen_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out broj1))
            {
                textBox1.Text = Math.Sqrt(broj1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
                textBox1.Clear();
            }
        }
    }
}
