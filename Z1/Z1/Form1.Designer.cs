﻿using System;

namespace Z1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cosinus = new System.Windows.Forms.Button();
            this.tangens = new System.Windows.Forms.Button();
            this.cotangens = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.sinus = new System.Windows.Forms.Button();
            this.korjen = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.podijeljeno = new System.Windows.Forms.Button();
            this.puta = new System.Windows.Forms.Button();
            this.apsolutna = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(28, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(114, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cosinus
            // 
            this.cosinus.Location = new System.Drawing.Point(199, 67);
            this.cosinus.Name = "cosinus";
            this.cosinus.Size = new System.Drawing.Size(34, 23);
            this.cosinus.TabIndex = 8;
            this.cosinus.Text = "cos";
            this.cosinus.UseVisualStyleBackColor = true;
            this.cosinus.Click += new System.EventHandler(this.cosinus_Click);
            // 
            // tangens
            // 
            this.tangens.Location = new System.Drawing.Point(239, 67);
            this.tangens.Name = "tangens";
            this.tangens.Size = new System.Drawing.Size(34, 23);
            this.tangens.TabIndex = 9;
            this.tangens.Text = "tan";
            this.tangens.UseVisualStyleBackColor = true;
            this.tangens.Click += new System.EventHandler(this.tangens_Click);
            // 
            // cotangens
            // 
            this.cotangens.Location = new System.Drawing.Point(279, 67);
            this.cotangens.Name = "cotangens";
            this.cotangens.Size = new System.Drawing.Size(34, 23);
            this.cotangens.TabIndex = 10;
            this.cotangens.Text = "ctg";
            this.cotangens.UseVisualStyleBackColor = true;
            this.cotangens.Click += new System.EventHandler(this.cotangens_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(239, 109);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(74, 23);
            this.button10.TabIndex = 11;
            this.button10.Text = "=";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // sinus
            // 
            this.sinus.Location = new System.Drawing.Point(159, 67);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(34, 23);
            this.sinus.TabIndex = 12;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // korjen
            // 
            this.korjen.Location = new System.Drawing.Point(159, 109);
            this.korjen.Name = "korjen";
            this.korjen.Size = new System.Drawing.Size(34, 23);
            this.korjen.TabIndex = 13;
            this.korjen.Text = "√";
            this.korjen.UseVisualStyleBackColor = true;
            this.korjen.Click += new System.EventHandler(this.korjen_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(159, 21);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(34, 23);
            this.plus.TabIndex = 14;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(199, 20);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(34, 23);
            this.minus.TabIndex = 15;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // podijeljeno
            // 
            this.podijeljeno.Location = new System.Drawing.Point(279, 20);
            this.podijeljeno.Name = "podijeljeno";
            this.podijeljeno.Size = new System.Drawing.Size(34, 23);
            this.podijeljeno.TabIndex = 16;
            this.podijeljeno.Text = "/";
            this.podijeljeno.UseVisualStyleBackColor = true;
            this.podijeljeno.Click += new System.EventHandler(this.podijeljeno_Click);
            // 
            // puta
            // 
            this.puta.Location = new System.Drawing.Point(239, 20);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(34, 23);
            this.puta.TabIndex = 17;
            this.puta.Text = "*";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.puta_Click);
            // 
            // apsolutna
            // 
            this.apsolutna.Location = new System.Drawing.Point(199, 109);
            this.apsolutna.Name = "apsolutna";
            this.apsolutna.Size = new System.Drawing.Size(34, 23);
            this.apsolutna.TabIndex = 18;
            this.apsolutna.Text = "|⬚|";
            this.apsolutna.UseVisualStyleBackColor = true;
            this.apsolutna.Click += new System.EventHandler(this.apsolutna_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 189);
            this.Controls.Add(this.apsolutna);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.podijeljeno);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.korjen);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.cotangens);
            this.Controls.Add(this.tangens);
            this.Controls.Add(this.cosinus);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button cosinus;
        private System.Windows.Forms.Button tangens;
        private System.Windows.Forms.Button cotangens;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button korjen;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button podijeljeno;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.Button apsolutna;
    }
}

